import sys
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import Perceptron
from sklearn.pipeline import Pipeline
from sklearn.datasets import load_files
from sklearn.model_selection import train_test_split
from sklearn import metrics
import tweepy
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import sys
import json
from textblob import TextBlob
from textblob.sentiments import NaiveBayesAnalyzer
import csv 

#TRAIN THE CLASSIFIER
# The training data folder must be passed as first argument
languages_data_folder = sys.argv[1]
dataset = load_files(languages_data_folder)

# Split the dataset in training and test set:
docs_train, docs_test, y_train, y_test = train_test_split(
    dataset.data, dataset.target, test_size=0.5)
#Changed test_size from 0.5 to 0.3


# Build a vectorizer that splits strings into sequence of 1 to 3
# characters instead of word tokens
vectorizer = TfidfVectorizer(ngram_range=(1, 3), analyzer='char',
                             use_idf=False)

# Build a vectorizer / classifier pipeline using the previous analyzer
# the pipeline instance should stored in a variable named clf
clf = Pipeline([
    ('vec', vectorizer),
    ('clf', Perceptron(tol=1e-3)),
])

# Fit the pipeline on the training set
clf.fit(docs_train, y_train)

# Predict the outcome on the testing set in a variable named y_predicted
y_predicted = clf.predict(docs_test)

# Print the classification report
print(metrics.classification_report(y_test, y_predicted,
                                    target_names=dataset.target_names))

# Plot the confusion matrix
cm = metrics.confusion_matrix(y_test, y_predicted)
print(cm)


###SEARCH FOR TWEETS###
class listener(StreamListener):
    #Look for tweets with keywords included
    def on_data(self, data):
        global sentences
        global count, out
        global violent_crime, acquisitive_crime, racial_crime, drug_crime, all_crime
        
        #Set mapping for uncodable characters (emojis)
        non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)
        try:
            all_data = json.loads(data)
        except Exception as e:
            return

        try:
            tweet = all_data["text"]
        except KeyError as e:
            return
        
        username = all_data["user"]["screen_name"]

        #Filter out retweets
        if all_data["retweeted"] == True:
            return
        if "RT" in tweet.split():
            return

        #Determine which type of crime the tweet belongs to
        overlap = self.filter(tweet)
        if overlap > 1:
            print("KEYWORDS IN COMMON: " + str(overlap))
            
            #Process Tweet with TextBlob
            opinion = TextBlob(tweet, analyzer=NaiveBayesAnalyzer())
            #print(opinion.sentiment)

            #Add tweet to the sentences list
            sentences.append(tweet)

            #open and write to output file
            #out.write(tweet.encode('utf-8'))
            #out.write(('\n').encode('utf-8'))
            print (username, " :: ", tweet.translate(non_bmp_map))
            #print(len(sentences))
            count = count + 1
                
        #Stop when 50 tweets have been reached
        if len(sentences) > 5:
            #print("IN IF STATEMENT!!")
            predicted = clf.predict(sentences)
            for s, p in zip(sentences, predicted):
                #print("IN FOR LOOOOP")
                print(dataset.target_names[p])
                if dataset.target_names[p] == "y":
                    #print("IN SECOND IF")
                    out.write(s.encode('utf-8'))
                    out.write(('\n').encode('utf-8'))
                    print("WROTE SUCCESSFULLY")
                    print(u'TWEET:  "%s": "%s"' % (s, dataset.target_names[p]))
            out.close()
            exit(0)
        return True

    def on_error(self, status):
        print (status)


    def filter(self, tweet):
        #Filter tweet based on keywords
        #Consider violent_crime, acquisitive_crime, racial_crime, drug_crime, all_crime
        all_count = common_words(tweet, all_crime)
        violent_count = common_words(tweet, violent_crime)
        acq_count = common_words(tweet, acquisitive_crime)
        racial_count = common_words(tweet, racial_crime)
        drug_count = common_words(tweet, drug_crime)

        max_overlap = max([all_count, violent_count, acq_count, racial_count, drug_count])
        #print("FILTER: " + str(max_overlap))

        return max_overlap

def common_words(tweet, keywords):
    n = 0
    for item in keywords:
        i = 1
        words1 = set((tweet.split()))
        for word in item:
            if word in words1:
                #print("WORD" + str(i) + ": " + str(word))
                n += 1
                i += 1
                break
    return n

        

auth = tweepy.OAuthHandler('vSPxLiUmfcstOrKL6YIVPwcPY', 'zPmB5tul0HVnetfrP8z1iAtR61cCPcexn6za8lf6YaioNcvXex')
auth.set_access_token('874469976152850433-PDT7mjKaGShq7JOA7A934VVBoLu61BZ',
                      'KoO5bwHwausYA1l9r8WDUxMbwQE3Z3KmkrFA0lEMmBIrV')
api = tweepy.API(auth)
count = 0
sentences = list()

violent_crime = [["violence", "stabbed", "stabbing", "knife attack", "punched",
                  "assault", "robbery", "homicide", "wounding", "domestic violence",
                  "mugging", "murder", "murdered", "killed", "manslaughter",
                  "infanticide", "sexual assault", "rape", "drink"], ["driving", "drug"],
                  ["driving", "GBH", "possession"], ["weapons", "harassment", "firearm",
                  "stalking"]]

acquisitive_crime = [ ["theft", "burglary", "vehicle"], ["stolen", "taken", "robbery",
                      "breaking"], ["entering", "bike", "car"]]

racial_crime = [["race", "racial"], ["abuse", "racist", "hate crime", "religion", "hatred",
                "religious"], ["aggravated"]]

drug_crime = [["drugs", "drug"],["possession", "intent to supply", "cannabis", "classa",
              "class b", "class c", "drug trafficking", "heroin", "marijuana", "pot", "cocaine", "LSD",
              "amphetamine", "ketamine"]]

all_crime = [["violence", "stabbed", "stabbing", "knife attack", "punched",
             "assault", "robbery", "homicide", "wounding", "domestic violence",
             "mugging", "murder", "murdered", "killed", "manslaughter",
             "infanticide", "sexual assault", "rape", "drink"], ["driving", "drug"],
             ["driving", "GBH", "possession" + "weapons", "harassment", "firearm",
             "stalking", "theft", "burglary", "vehicle"], ["stolen", "nicked",
             "robbery", "breaking"], ["entering", "bike"], ["stolen", "vandalism",
             "vandalised", "vandal", "arson", "graffiti", "deliberate damage",
             "malicious", "set fire", "criminal damage", "destroyed", "damaged",
             "keying"], ["car", "reckless", "fraud", "forgery", "identity theft", "bankcard"],
             ["stolen", "false accounting", "bankruptcy", "credit card",
             "debit card", "bank details"], ["stolen", "fraudulent", "racial", "racial"],
             ["abuse", "racist", "hate crime", "religion", "hatred", "religious"],
             ["aggravated", "drugs", "drug"], ["possession", "intent to supply",
             "cannabis", "class a", "class b", "class c", "drug trafficking", "heroin",
             "cocaine", "LSD", "amphetamine", "ketamine"]]

keywords = ["violence", "stabbed", "stabbing", "knife attack", "punched",
            "assault", "robbery", "homicide", "wounding", "domestic violence",
            "mugging", "murder", "murdered", "killed", "manslaughter",
            "infanticide", "sexual assault", "rape", "drink", "drugs", "drug", "race", "racial",
            "theft", "burglary", "vehicle", "violence", "stabbed", "stabbing", "knife attack",
            "punched","assault", "robbery", "homicide", "wounding", "domestic violence",
            "mugging", "murder", "murdered", "killed", "manslaughter","infanticide",
            "sexual assault", "rape", "drink"]

            
out = open('seattle_tweets.txt', 'wb')
twitterStream = Stream(auth, listener())
twitterStream.filter(track=keywords, locations=[-122.2734,47.2930,-122.1327,47.4402], languages=["en"])

#Determine if tweets are crime related

#If crime related classify based on type of crime





