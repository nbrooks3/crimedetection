# This script is used to download the crime report data from data.seattle.gov.

# It can also be used to filter the csv data by column, offense type,
# year.

# Lastly, it can be used to sort the data by date or by crime.

# To use each of these functionalities, comment out the other portions shown
# below and change the hardcoded values to reflect which columns, crimes, or
# dates you'd like to filter and sort by.

#!/usr/bin/env python
  
import sys
import os
import csv
from collections import defaultdict

def getopts(argv):
    opts = []
    while argv:
        if argv [0][0] == '-':
            opts.append(argv[0])
        argv = argv[1:]
    return opts

def usage():
    print ("")
    print ("    Usage: python3 seattle.py")
    print ("    Options*: -h -d -s")
    print ("    -----------------")
    print ("    -h displays usage")
    print ("    -d downloads most current data")
    print ("    -s sorts, filters data using hardcoded parameters")
    print ("")
    print ("    *Note: multiple options can be used concurrently")
    print ("")

def downloaddata():
    print ("Downloading data...")
    os.system("wget https://data.seattle.gov/api/views/7ais-f98f/rows.csv")
    os.system("mv rows.csv seattledata.csv")
    print ("Downloaded data!")

def sortdata():
    print ("Filtering data...")

    '''    #   Get lists of columns
    columns = defaultdict(list)
    with open('seattledata.csv') as f:
        reader = csv.DictReader(f)
        for row in reader:d
            for (k,v) in row.items():
                columns[k].append(v)
    print (columns['Offense Code']) '''

    '''    # Filter by offense type
    with open('seattledata.csv', 'r') as fin, open('outfile.csv', 'w') as fout:
        writer = csv.writer(fout, delimiter=',')
        count = 0
        for row in csv.reader(fin, delimiter = ','):
            if count == 0:
                writer.writerow(row)
                count += 1                
            if "THEFT" in row[4]:
                writer.writerow(row)'''

    # Filters by year
    with open('seattledata.csv', 'r') as fin, open('filtereddata.csv', 'w') as fout:
        writer = csv.writer(fout, delimiter=',')
        count = 0
        for row in csv.reader(fin, delimiter = ','):
            if count == 0:
                writer.writerow(row)
                count += 1                
            if "2018" in row[7]: # 2018 is the chosen year
                writer.writerow(row)
    print("Filtered data!")
    print("Sorting data...")

    # Sort data by crime reported
    os.system("sort filtereddata.csv --field-separator=',' --key=7 > filteredandsorteddata.csv")    

    # Sort data by date reported
    # os.system("sort outfile.csv --field-separator=',' --key=8 > sorteddata.csv")    

    print ("Sorted data!")

if __name__ == "__main__":
    from sys import argv
    myargs = getopts(argv)
    if not myargs or '-h' in myargs:
        usage() 
    if '-d' in myargs:
        downloaddata()
    if '-s' in myargs:    
        sortdata()
