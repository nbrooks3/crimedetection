# seattle.py
seattle.py is used to download the crime report data from data.seattle.gov. It can also be used to filter the csv data by column, offense type, and year. Lastly, it can be used to sort the data by date or by crime.

To use each of these functionalities, comment oout the other portions shown
below and change the hardcoded values to reflect which columns, crimes, or
dates you'd like to filter and sort by.

To run seattle.py and display the usage message, run the following command: `./seattle.py -h`

To run seattle.py and download the current data, run: `./seattle.py -d`

To run seattle.py and filter and sort the data using the currently hardcoded parameters, run: `./seattle.py -s`

# geocoder.py
geocoder.py takes in a .csv of longitude and latitude coordinates and produces
zip codes using the Google Maps reverse geocoder API. This was ultimately
not used because we used a heatmap service that uses the coordinates themselves
rather than zipcodes, which the first service we'd selected requred.

To run geocoder.py, save your coordinates in a .csv file named `longsandlats.csv`
with the longitude coordinates in the first column and latitude coordinates
in the second. Then, run the following command: `./geocoder.py`
