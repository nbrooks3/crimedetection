# Takes in a csv of longitude and latitude coordinates and produces zip codes
# using the Google Maps reverse geocoder API.

# This was ultimately not used because we used a heatmap service that uses
# the coordinates themselves rather than zipcodes, which the first service
# we'd selected required.

#!/usr/bin/env python
  
import requests
import pandas

def example(latitude, longitude):
    sensor = 'true'
   
    base = "http://maps.googleapis.com/maps/api/geocode/json?"
    params = "latlng={lat},{lon}&sensor={sen}".format(
        lat=latitude,
        lon=longitude,
        sen=sensor
    )
    url = "{base}{params}".format(base=base, params=params)
    while True:
        response = requests.get(url)
        if (response.json()['status'] == 'OK'):
            info = response.json()['results'][0]['formatted_address']
            return info.split(',')[2][4:]

    return 00000
            
if __name__ == "__main__":
    file = open('longsandlats.csv', 'r+')
    file2 = open('formattedlls.txt', 'w')
    count = 0
    for line in file:
        count += 1
        line = line.strip("\n")
        longitude = line.split(',')[0]
        latitude = line.split(',')[1]
        print (str(count) + " conversions completed!")
        file2.write("new google.maps.LatLng(" + latitude + ", " + longitude + ")," + "\n")
    file.close()
    file2.close()
