Crime Detection using Social Media

Brittany DiGenova, Nikolas Dean Brooks

Social-Sensing and Cyberphysical Systems, Spring 2018

#SVM Model
In order to run the SVM model need a directory with subdirectories of the various classes the SVM will classify text into.
These subdirectories must be filled with .txt files with relevant text for the subcategory.
In this case the directory is "crime" and subdirectories are "y" for yes it is crime. And "n" for no crime. To run the SVM 
on the crime tweets use the following command:

python SVMmodel.py crime
