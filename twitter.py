import tweepy
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import sys
import json
from textblob import TextBlob
from textblob.sentiments import NaiveBayesAnalyzer

class listener(StreamListener):
    #Look for tweets with keywords included
    def on_data(self, data):
        global count, out
        global violent_crime, acquisitive_crime, racial_crime, drug_crime, all_crime
        
        #Set mapping for uncodable characters (emojis)
        non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)
        try:
            all_data = json.loads(data)
        except Exception as e:
            return

        try:
            tweet = all_data["text"]
        except KeyError as e:
            return
        
        username = all_data["user"]["screen_name"]

        #Filter out retweets
        if all_data["retweeted"] == True:
            return
        if "RT" in tweet.split():
            return

        #Determine which type of crime the tweet belongs to
        overlap = self.filter(tweet)
        if overlap > 1:
            print("WORDS IN COMMON: " + str(overlap))
            
            #Process Tweet with TextBlob
            opinion = TextBlob(tweet, analyzer=NaiveBayesAnalyzer())
            print(opinion.sentiment)

            #open and write to output file
            #out.write(tweet.encode('utf-8'))
            #out.write(('\n').encode('utf-8'))
            print (username, " :: ", tweet.translate(non_bmp_map))
            count = count + 1
                
        #Stop when 50 tweets have been reached
        if count > 500:
            out.close()
            exit(0)
        return True

    def on_error(self, status):
        print (status)


    def filter(self, tweet):
        #Filter tweet based on keywords
        #Consider violent_crime, acquisitive_crime, racial_crime, drug_crime, all_crime
        all_count = common_words(tweet, all_crime)
        violent_count = common_words(tweet, violent_crime)
        acq_count = common_words(tweet, acquisitive_crime)
        racial_count = common_words(tweet, racial_crime)
        drug_count = common_words(tweet, drug_crime)

        max_overlap = max([all_count, violent_count, acq_count, racial_count, drug_count])
        #print("FILTER: " + str(max_overlap))

        return max_overlap

def common_words(tweet, keywords):
    n = 0
    for item in keywords:
        i = 1
        words1 = set((tweet.split()))
        for word in item:
            if word in words1:
                print("WORD" + str(i) + ": " + str(word))
                n += 1
                i += 1
                break
    return n

        

auth = tweepy.OAuthHandler('vSPxLiUmfcstOrKL6YIVPwcPY', 'zPmB5tul0HVnetfrP8z1iAtR61cCPcexn6za8lf6YaioNcvXex')
auth.set_access_token('874469976152850433-PDT7mjKaGShq7JOA7A934VVBoLu61BZ',
                      'KoO5bwHwausYA1l9r8WDUxMbwQE3Z3KmkrFA0lEMmBIrV')
api = tweepy.API(auth)
count = 0

violent_crime = [["violence", "stabbed", "stabbing", "knife attack", "punched",
                  "assault", "robbery", "homicide", "wounding", "domestic violence",
                  "mugging", "murder", "murdered", "killed", "manslaughter",
                  "infanticide", "sexual assault", "rape", "drink"], ["driving", "drug"],
                  ["driving", "GBH", "possession"], ["weapons", "harassment", "firearm",
                  "stalking"]]

acquisitive_crime = [ ["theft", "burglary", "vehicle"], ["stolen", "taken", "robbery",
                      "breaking"], ["entering", "bike", "car"]]

racial_crime = [["race", "racial"], ["abuse", "racist", "hate crime", "religion", "hatred",
                "religious"], ["aggravated"]]

drug_crime = [["drugs", "drug"],["possession", "intent to supply", "cannabis", "classa",
              "class b", "class c", "drug trafficking", "heroin", "marijuana", "pot", "cocaine", "LSD",
              "amphetamine", "ketamine"]]

all_crime = [["violence", "stabbed", "stabbing", "knife attack", "punched",
             "assault", "robbery", "homicide", "wounding", "domestic violence",
             "mugging", "murder", "murdered", "killed", "manslaughter",
             "infanticide", "sexual assault", "rape", "drink"], ["driving", "drug"],
             ["driving", "GBH", "possession" + "weapons", "harassment", "firearm",
             "stalking", "theft", "burglary", "vehicle"], ["stolen", "nicked",
             "robbery", "breaking"], ["entering", "bike"], ["stolen", "vandalism",
             "vandalised", "vandal", "arson", "graffiti", "deliberate damage",
             "malicious", "set fire", "criminal damage", "destroyed", "damaged",
             "keying"], ["car", "reckless", "fraud", "forgery", "identity theft", "bankcard"],
             ["stolen", "false accounting", "bankruptcy", "credit card",
             "debit card", "bank details"], ["stolen", "fraudulent", "racial", "racial"],
             ["abuse", "racist", "hate crime", "religion", "hatred", "religious"],
             ["aggravated", "drugs", "drug"], ["possession", "intent to supply",
             "cannabis", "class a", "class b", "class c", "drug trafficking", "heroin",
             "cocaine", "LSD", "amphetamine", "ketamine"]]

keywords = ["violence", "stabbed", "stabbing", "knife attack", "punched",
            "assault", "robbery", "homicide", "wounding", "domestic violence",
            "mugging", "murder", "murdered", "killed", "manslaughter",
            "infanticide", "sexual assault", "rape", "drink", "drugs", "drug", "race", "racial",
            "theft", "burglary", "vehicle", "violence", "stabbed", "stabbing", "knife attack",
            "punched","assault", "robbery", "homicide", "wounding", "domestic violence",
            "mugging", "murder", "murdered", "killed", "manslaughter","infanticide",
            "sexual assault", "rape", "drink"]

            
out = open('seattle_tweets.txt', 'wb')
twitterStream = Stream(auth, listener())
twitterStream.filter(track=keywords, locations=[-122.2734,47.2930,-122.1327,47.4402], languages=["en"])

#Determine if tweets are crime related

#If crime related classify based on type of crime
